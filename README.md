# JAX-WS Client Example

Let's build a JAX-WS client for [Europe PMC SOAP web service](http://europepmc.org/SoapWebServices).

## Test web service

Load the [web service WSDL](https://www.ebi.ac.uk/europepmc/webservices/soap?wsdl) in a WS client like SoapUI and take a look at the available methods. We'll be using `searchPublications` in this example.

## Create empty project

Generate an empty Java project. I'll use IntelliJ to generate a new Gradle project.

Copy [web service WSDL](https://www.ebi.ac.uk/europepmc/webservices/soap?wsdl) in `src/main/resources`; we'll use it to generate the Java code.

See results in commit a1103c2949ebcf22cf819c55f6959c1a43328e95.

## Generate JAX-WS client

Generate JAX-WS client with `wsdl2java`. This tool is part of Apache's CXF distribution, we can use [sdkman](https://sdkman.io/) to install it if needed.

### sdkman

Follow installation instructions from https://sdkman.io/install.

```shell script
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk version
```

### CXF

Once installed sdkman, installing CXF is just running one command.

```shell script
sdk install cxf
```

Check the installation running `wsdl2java -h`.

### wsdl2java

```shell script
wsdl2java -p com.gitlab.isalgueiro.examples.jaxws.client -client -d src/main/java/ src/main/resources/WSCitationImplPortBinding.wsdl
```

This will generate all the needed Java classes to implement this service.

- `-p` target package for the generated classes
- `-client` Generates starting point code for a web service client.
- `-d`  directory into which the generated code files are written.

See results in commit 6828eb13b2a1e2a3a280d628adc415b41a578479.

### Fixing stuff

Generated client will load the WSDL from the relative path from where we executed `wsdl2java`

```java
url = new URL("file:src/main/resources/WSCitationImplPortBinding.wsdl");
```

So this will fail when executing this client from any other directory

```shell script
Caused by: java.io.FileNotFoundException: src/main/resources/WSCitationImplPortBinding.wsdl (No such file or directory)
```

We already have the WSDL file in the classpath, so we can just the classloader to get it 

```java
public final static URL WSDL_LOCATION = ClassLoader.getSystemResource("WSCitationImplPortBinding.wsdl");
```

Fix in e5528a660d62dcf4916e1ac8192a86cf2ac402de. 

## Execute web service methods

`wsdl2java` already generated sample client code in `WSCitationImpl_WSCitationImplPort_Client`. We can use it as a guide on how to write our oun client code.

See results in df6a5f173ac173ac9b25f6232c92cd8b0480d2de.

At this point we can generate a jar that we can execute to run the remote web service methods. This jar will be very useful to validate our client code, to check connectivity between servers, etc.

```shell script
./gradlew build
java -jar build/libs/jax-ws-client-example-1.0-SNAPSHOT.jar
```

Or, using docker

```
FROM openjdk:8-jdk-alpine
ADD build/libs/jax-ws-client-example-1.0-SNAPSHOT.jar /app.jar
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT /entrypoint.sh
```

A simple entry-point script

```shell script
#!/bin/sh
java -jar /app.jar

```

```shell script
docker build . -t jax-ws-test
docker run jax-ws-test
```