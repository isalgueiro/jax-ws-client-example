package com.gitlab.isalgueiro.examples;

import com.gitlab.isalgueiro.examples.jaxws.client.QueryException_Exception;
import com.gitlab.isalgueiro.examples.jaxws.client.ResponseWrapper;
import com.gitlab.isalgueiro.examples.jaxws.client.WSCitationImpl;
import com.gitlab.isalgueiro.examples.jaxws.client.WSCitationImplService;

import javax.xml.namespace.QName;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    private static final QName SERVICE_NAME = new QName("http://webservice.cdb.ebi.ac.uk/", "WSCitationImplService");

    public static void main(String[] args) {
        final URL wsdlURL;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) {
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        } else {
            wsdlURL = WSCitationImplService.WSDL_LOCATION;
        }

        final WSCitationImplService service = new WSCitationImplService(wsdlURL, SERVICE_NAME);
        final WSCitationImpl port = service.getWSCitationImplPort();
        System.out.println("Invoking searchPublications...");
        java.lang.String queryString = "vaccines";
        java.lang.String resultType = "";
        java.lang.String cursorMark = "*";
        java.lang.String pageSize = "";
        java.lang.String sort = "";
        java.lang.String synonym = "";
        java.lang.String email = "";
        try {
            ResponseWrapper result = port.searchPublications(queryString, resultType, cursorMark, pageSize, sort, synonym, email);
            System.out.println("Número de resultados " + result.getResultList().getResult().size());

        } catch (QueryException_Exception e) {
            System.out.println("Expected exception: QueryException has occurred.");
            System.out.println(e.toString());
        }

    }
}
