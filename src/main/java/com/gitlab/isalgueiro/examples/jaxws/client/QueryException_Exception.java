
package com.gitlab.isalgueiro.examples.jaxws.client;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.5
 * 2019-09-18T14:39:29.011+02:00
 * Generated source version: 3.2.5
 */

@WebFault(name = "QueryException", targetNamespace = "http://webservice.cdb.ebi.ac.uk/")
public class QueryException_Exception extends Exception {

    private com.gitlab.isalgueiro.examples.jaxws.client.QueryException queryException;

    public QueryException_Exception() {
        super();
    }

    public QueryException_Exception(String message) {
        super(message);
    }

    public QueryException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public QueryException_Exception(String message, com.gitlab.isalgueiro.examples.jaxws.client.QueryException queryException) {
        super(message);
        this.queryException = queryException;
    }

    public QueryException_Exception(String message, com.gitlab.isalgueiro.examples.jaxws.client.QueryException queryException, java.lang.Throwable cause) {
        super(message, cause);
        this.queryException = queryException;
    }

    public com.gitlab.isalgueiro.examples.jaxws.client.QueryException getFaultInfo() {
        return this.queryException;
    }
}
