FROM openjdk:8-jdk-alpine
ADD build/libs/jax-ws-client-example-1.0-SNAPSHOT.jar /app.jar
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT /entrypoint.sh
